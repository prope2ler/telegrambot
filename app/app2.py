#!/usr/bin/env python3
# Use this token to access the HTTP API:
# 181708882:AAHYyNpWo1gbZw6do8LJoYTnL9LoO5tUmks

import telebot
from telebot import apihelper
bot = telebot.TeleBot('181708882:AAHYyNpWo1gbZw6do8LJoYTnL9LoO5tUmks')

# apihelper.proxy = {'https':'https://51.79.142.25:8080'} #it's working code for proxy

'''
@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.text == 'Hello':
        bot.send_message(message.from_user.id, 'Hi, could i help you?')
    elif message.text == '/help':
        bot.send_message(message.from_user.id, 'Pls, type Hello')
    else:
        bot.send_message(message.from_user.id, 'I cant understand you')

bot.polling(none_stop=True, interval=0)
'''

mainDict = {}

# Обработчик команд '/start' и '/help'.
@bot.message_handler(commands=['start', 'help'])
def handle_start_help(message):
    pass

#Обработчик сообщений, подходящих под указанное регулярное выражение
@bot.message_handler(regexp="\d\d/\d\d/\d{4}")
def handle_message(message):
    bot.send_message(message.chat.id, 'бинго: ' + message.text)
    mainDict['date'] = message.text
    bot.send_message(message.chat.id, 'из словаря: ' + mainDict['date'])


@bot.message_handler(func=lambda message: True, content_types=['text'])
def echo_msg(message):
    bot.send_message(message.chat.id, 'это авто-эхо чатбота: ' + message.text)

'''
# Обработчик для документов и аудиофайлов
@bot.message_handler(content_types=['document', 'audio'])
def handle_docs_audio(message):
    pass

# Обработчик сообщений, содержащих документ с mime_type 'text/plain' (обычный текст)
@bot.message_handler(func=lambda message: message.document.mime_type == 'text/plain', content_types=['document'])
def handle_text_doc(message):
    pass
'''
    

# Запуск программы
if __name__ == '__main__':
    bot.polling(none_stop=True)

