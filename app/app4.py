#!/usr/bin/env python3
# Use this token to access the HTTP API:
# 181708882:AAHYyNpWo1gbZw6do8LJoYTnL9LoO5tUmks

import telebot
from telebot import apihelper
from telebot import types
bot = telebot.TeleBot('181708882:AAHYyNpWo1gbZw6do8LJoYTnL9LoO5tUmks')


# Обработчик команд '/start' и '/help'.
@bot.message_handler(commands=['start', 'help'])
def handle_start_help(message):
    markup = types.ReplyKeyboardMarkup()
    markup.row('the first row')
    markup.row('the second row')
    bot.send_message(message.chat.id, 'Choose one answer:', reply_markup=markup)

@bot.message_handler(func=lambda message: True, content_types=['text'])
def echo_msg(message):
    bot.send_message(message.chat.id, 'это авто-эхо чатбота: ' + message.text)    

# Запуск программы
if __name__ == '__main__':
    bot.polling(none_stop=True)

